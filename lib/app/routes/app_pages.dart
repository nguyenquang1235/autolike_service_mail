import 'package:get/get.dart';

import 'package:autolike_service_mail/app/modules/home/bindings/home_binding.dart';
import 'package:autolike_service_mail/app/modules/home/views/home_view.dart';
import 'package:autolike_service_mail/app/modules/puppeteer/bindings/puppeteer_binding.dart';
import 'package:autolike_service_mail/app/modules/puppeteer/views/puppeteer_view.dart';
import 'package:autolike_service_mail/app/modules/test/bindings/test_binding.dart';
import 'package:autolike_service_mail/app/modules/test/views/test_view.dart';

part 'app_routes.dart';

class AppPages {
  AppPages._();

  static const INITIAL = Routes.PUPPETEER;

  static final routes = [
    GetPage(
      name: _Paths.HOME,
      page: () => HomeView(),
      binding: HomeBinding(),
    ),
    GetPage(
      name: _Paths.TEST,
      page: () => TestView(),
      binding: TestBinding(),
    ),
    GetPage(
      name: _Paths.PUPPETEER,
      page: () => PuppeteerView(),
      binding: PuppeteerBinding(),
    ),
  ];
}
