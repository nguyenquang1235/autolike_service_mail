import 'package:get/get.dart';
import 'package:puppeteer/puppeteer.dart';

class PuppeteerController extends GetxController {
  //TODO: Implement PuppeteerController
  var browser;
  var myPage;

  @override
  void onInit() {
    super.onInit();
    init();
  }

  @override
  void onReady() {
    super.onReady();
  }

  @override
  void onClose() {}

  init() async {
    browser = await puppeteer.connect();
    myPage = await browser.newPage();
    await myPage.goto('https://outlook.live.com/owa/?nlp=1&signup=1',
        wait: Until.networkIdle);
  }
}
