import 'package:get/get.dart';

import '../controllers/puppeteer_controller.dart';

class PuppeteerBinding extends Bindings {
  @override
  void dependencies() {
    // Get.lazyPut<>(
    //   () => PuppeteerController(),
    // );

    Get.put(PuppeteerController());
  }
}
