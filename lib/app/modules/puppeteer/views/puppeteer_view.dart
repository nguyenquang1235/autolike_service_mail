import 'package:flutter/material.dart';

import 'package:get/get.dart';

import '../controllers/puppeteer_controller.dart';

class PuppeteerView extends GetView<PuppeteerController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Puppeteer'),
        centerTitle: true,
      ),
      body: Center(
        child: Text(
          'PuppeteerView is working',
          style: TextStyle(fontSize: 20),
        ),
      ),
    );
  }
}
