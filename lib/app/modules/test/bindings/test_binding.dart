import 'package:autolike_service_mail/app/modules/home/controllers/home_controller.dart';
import 'package:get/get.dart';

import '../controllers/test_controller.dart';

class TestBinding extends Bindings {
  @override
  void dependencies() {
    Get.lazyPut<TestController>(
      () => TestController(),
    );
    // Get.put(HomeController());
  }
}
