import 'package:flutter/material.dart';
import 'package:flutter_inappwebview/flutter_inappwebview.dart';

import 'package:get/get.dart';

import '../controllers/home_controller.dart';

class HomeView extends GetView<HomeController> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text('HomeView'),
          centerTitle: true,
        ),
        body: Column(
          children: [
            Expanded(
              child: InAppWebView(
                initialUrlRequest: URLRequest(
                    url: Uri.parse(
                        "https://outlook.live.com/owa/?nlp=1&signup=1")),
                initialOptions: InAppWebViewGroupOptions(
                    crossPlatform: InAppWebViewOptions(
                        javaScriptEnabled: true,
                        allowUniversalAccessFromFileURLs: true,
                        allowFileAccessFromFileURLs: true,
                        javaScriptCanOpenWindowsAutomatically: true)),
                // onWebViewCreated: controller.hotmail.onCreate,
                onLoadStop: controller.hotmail.onLoaded,
                onProgressChanged: controller.hotmail.onLoading,
                onWebViewCreated: (InAppWebViewController handleController) {
                  controller.hotmail.onCreate(handleController);
                },
              ),
            ),
            ElevatedButton(
                onPressed: () {
                  controller.hotmail
                      .testPostMessage(controller.hotmail.controller);
                },
                child: Text("Test"))
          ],
        ));
  }
}
