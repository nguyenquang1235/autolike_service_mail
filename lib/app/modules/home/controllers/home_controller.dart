import 'dart:async';
import 'dart:developer';
import 'dart:io';
import 'dart:math';

import 'package:flutter_inappwebview/flutter_inappwebview.dart';
import 'package:get/get.dart';
// import 'dart:convert';
// import 'package:flutter/services.dart' show rootBundle;
import 'package:autolike_service_mail/app/data/const_data.dart' as const_data;

class HomeController extends GetxController {
  final hotmail = HotMail();
  // final ChromeSafariBrowser browser = ChromeSafariBrowser();
  @override
  void onInit() {
    super.onInit();
    // browser.open(
    //     url: Uri.parse("https://outlook.live.com/owa/?nlp=1&signup=1"),
    //     options: ChromeSafariBrowserClassOptions(
    //         android: AndroidChromeCustomTabsOptions(
    //             showTitle: false,
    //             addDefaultShareMenuItem: false),
    //         ios: IOSSafariOptions(barCollapsingEnabled: true)));
  }

  @override
  void onReady() {
    // super.onReady();
  }

  @override
  void onClose() {}

  Future loadInitData() async {}
}

class HotMail {
  List<String> get listFirstName => const_data.firstName;
  List<String> get listLastName => const_data.lastName;

  late InAppWebViewController controller;
  late Completer<String>? completer;
  bool completerInited = false;
  RxDouble loading = 0.0.obs;

  bool pageChanged = true;

  String? firstName;
  String? lastName;

  void onCreate(InAppWebViewController handleControl) async {
    // controller = handleControl;
    handleControl.addJavaScriptHandler(
        handlerName: "handleChangePage",
        callback: (values) {
          pageChanged = values[0];
        });
  }

  void onLoading(InAppWebViewController control, int percent) async {
    loading.value = percent.toDouble() / 100;
  }

  Future<FormResubmissionAction?> onSubmit(
      InAppWebViewController control, Uri? percent) async {
    return FormResubmissionAction.RESEND;
  }

  Future onLoaded(InAppWebViewController control, Uri? uri) async {
    controller = control;
    pageChanged = true;
    if (completerInited && !completer!.isCompleted) {
      final html = await control.getHtml();
      completer!.complete(html);
    }

    // control.evaluateJavascript(source: '''
    //                             var username = document.getElementById('MemberName');
    //                             var phoneCountryCode = document.getElementById('LiveDomainBoxList');
    //                             var phonerCountryCodeLabel = document.getElementsByClassName('phoneCountryCode');

    //                             username.value = '${randomUsername()}'
    //                             phoneCountryCode.value = 'hotmail.com';
    //                             phonerCountryCodeLabel[1].innerText = '@' + phoneCountryCode.value;

    //                             phoneCountryCode.dispatchEvent(new Event("change"));
    //                             username.dispatchEvent(new Event("change"));

    //                             OnNext();
    //                        ''');
    // sleep(Duration(seconds: 2));
    // control.evaluateJavascript(source: '''
    //                             var password = document.getElementById('PasswordInput')
    //                             password.value = 'Cgb123@456';
    //                             password.dispatchEvent(new Event("change"));
    //                             OnNext();
    //                        ''');
    // sleep(Duration(seconds: 2));
    // control.evaluateJavascript(source: '''
    //                             var lastName = document.getElementById('LastName')
    //                             var firstName = document.getElementById('FirstName')
    //                             lastName.value = '$lastName';
    //                             firstName.value = '$firstName';

    //                             lastName.dispatchEvent(new Event("change"));
    //                             firstName.dispatchEvent(new Event("change"));

    //                             OnNext();
    //                        ''');
    // sleep(Duration(seconds: 2));
    // control.evaluateJavascript(source: '''
    //                             var birthday = document.getElementById('BirthDay');
    //                             var birthMonth = document.getElementById('BirthMonth');
    //                             var birthYear = document.getElementById('BirthYear');

    //                             birthday.value = '3';
    //                             birthMonth.value = '12';
    //                             birthYear.value = '1994';

    //                             birthday.dispatchEvent(new Event("change"));
    //                             birthMonth.dispatchEvent(new Event("change"));
    //                             birthYear.dispatchEvent(new Event("change"));

    //                             OnNext();

    //                        ''');
    // sleep(Duration(seconds: 10));

    // Future.delayed(Duration(milliseconds: 3000),
    //     () => control.evaluateJavascript(source: '''
    //         document.getElementById('enforcementFrame').contentWindow.document.getElementById('fc-iframe-wrap').contentWindow.parent.postMessage(JSON.stringify({
    //             eventId: "challenge-complete",
    //             payload: {
    //                 sessionToken: '4946169d28fb77f59.0725800101|r=us-east-1|metabgclr=%23ffffff|maintxtclr=%231B1B1B|mainbgclr=%23ffffff|guitextcolor=%23747474|metaiconclr=%23757575|meta_height=325|meta=7|lang=en|pk=B7D8911C-5CC8-A9A3-35B0-554ACEE604DA|at=40|ht=1|atp=2|cdn_url=https%3A%2F%2Fclient-api.arkoselabs.com%2Fcdn%2Ffc|lurl=https%3A%2F%2Faudio-us-east-1.arkoselabs.com|surl=https%3A%2F%2Fclient-api.arkoselabs.com'
    //             }
    //           }),'*');
    //            console.log('done');
    //       '''));
    // Future.delayed(Duration(milliseconds: 3000),
    //     () => control.evaluateJavascript(source: '''
    //                             while($pageChanged){
    //                               setTimeout(function() {
    //                                 if(document.getElementById('progressBarContent').style.display == 'none'){
    //                                   window.flutter_inappwebview.callHandler('handleChangePage', false);
    //                                 }
    //                               }, 3000);
    //                             }
    //                        '''));
    // while (pageChanged) {
    //   sleep(Duration(seconds: 2));
    //   String? html = await control.getHtml();
    //   if (html!.contains('home_children_button')) {
    //     pageChanged = false;
    //   }
    // }
    print('object');
  }

  Future<String> getHtml(String url) async {
    String definedURL;
    if (url.contains('www.')) {
      definedURL = url.replaceFirst('www.', 'mbasic.');
    } else if (url.contains('m.')) {
      definedURL = url.replaceFirst('m.', 'mbasic.');
    } else {
      definedURL = url;
    }
    completer = new Completer();
    completerInited = true;
    await controller.loadUrl(
        urlRequest: URLRequest(url: Uri.parse(definedURL)));
    // await controller.evaluateJavascript("(function(){Flutter.postMessage(window.document.body.outerHTML)})();");
    return completer!.future;
  }

  testPostMessage(InAppWebViewController control) {
    Future.delayed(Duration(milliseconds: 3000),
        () => control.evaluateJavascript(source: '''
            document.getElementById('enforcementFrame').contentWindow.document.getElementById('fc-iframe-wrap').contentWindow.parent.postMessage(JSON.stringify({
                eventId: "challenge-complete",
                payload: {
                    sessionToken: '4946169d28fb77f59.0725800101|r=us-east-1|metabgclr=%23ffffff|maintxtclr=%231B1B1B|mainbgclr=%23ffffff|guitextcolor=%23747474|metaiconclr=%23757575|meta_height=325|meta=7|lang=en|pk=B7D8911C-5CC8-A9A3-35B0-554ACEE604DA|at=40|ht=1|atp=2|cdn_url=https%3A%2F%2Fclient-api.arkoselabs.com%2Fcdn%2Ffc|lurl=https%3A%2F%2Faudio-us-east-1.arkoselabs.com|surl=https%3A%2F%2Fclient-api.arkoselabs.com'
                }
              }),'*');
               console.log('done');
          '''));
  }

  String randomUsername() {
    Random random = Random();
    firstName = (listFirstName..shuffle()).first;
    lastName = (listLastName..shuffle()).first;
    int randomNumber = random.nextInt(9999);
    return '${lastName?.toLowerCase()}${firstName?.toLowerCase()}$randomNumber';
  }
}
